package fr.corentin.urcalan.bonus_malus;

import fr.corentin.urcalan.Main;
import fr.corentin.urcalan.bonus_malus.commands.GetCommand;
import fr.corentin.urcalan.parents.IModule;
import fr.corentin.urcalan.utils.FileUtils;
import fr.skytale.commandlib.Commands;
import fr.skytale.jsonlib.JsonManager;

import java.io.File;
import java.io.IOException;

/**
 * Class {@link EffectsManager}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class EffectsManager implements IModule {

    private Effects effects;

    @Override
    public void enable() {
        FileUtils.createFiles("/effects.json");

        this.effects = loadJson("/effects.json");

        registerCommand();
        registerListener();
    }

    @Override
    public void disable() {
        this.effects.clear();
    }

    @Override
    public void registerCommand() {
        Commands commands = Main.getInstance().getCommandsManager();
        commands.registerCommand(new GetCommand());
    }

    @Override
    public void registerListener() {
    }

    public void saveJson(Effects effect, String path) {
        JsonManager jsonManager = Main.getInstance().getJsonManager();
        String stringPath = Main.getInstance().getDataFolder().getAbsolutePath() + path;
        File file = new File(stringPath);
        try {
            jsonManager.serialize(effect, Effects.class, file);
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't save Effect", e);
        }
    }

    public Effects loadJson(String path) {
        JsonManager jsonManager = Main.getInstance().getJsonManager();
        String filePath = Main.getInstance().getDataFolder().getAbsolutePath() + path;
        File file = new File(filePath);
        return jsonManager.deserialize(file, Effects.class);
    }

    public Effects getEffects() {
        return effects;
    }
}
