package fr.corentin.urcalan.bonus_malus.commands;

import fr.corentin.urcalan.Main;
import fr.corentin.urcalan.bonus_malus.EffectsManager;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.TargetCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Class {@link GetCommand}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class GetCommand extends TargetCommand<Player> {

    public static final String DESCRIPTION = "Commande permettant de récupérer un effet au hasard.";

    public GetCommand() {
        super(Player.class, "get");
        super.setPermission("urca.lan.get");
        super.addArgument("type");
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args) {
        super.setAutoCompleteValuesArg("type", Arrays.asList("bonus", "malus", "random"));
    }

    @Override
    protected boolean targetProcess(Commands commands, Player player, String... strings) {
        EffectsManager effectManager = Main.getInstance().getEffectManager();
        String type = super.getArgumentValue("type").toLowerCase();
        String effect;
        switch (type) {
            case "bonus":
                effect = effectManager.getEffects().getRandomBonus();
                if (Main.getInstance().isBroadcastEffects()) {
                    Bukkit.broadcastMessage(Main.PREFIX + "Le bonus choisi est : " + effect);
                } else {
                    player.sendMessage(Main.PREFIX + "Le bonus choisi est : " + effect);
                }
                break;
            case "malus":
                effect = effectManager.getEffects().getRandomMalus();
                if (Main.getInstance().isBroadcastEffects()) {
                    Bukkit.broadcastMessage(Main.PREFIX + "Le malus choisi est : " + effect);
                } else {
                    player.sendMessage(Main.PREFIX + "Le malus choisi est : " + effect);
                }
                break;
            case "random":
                effect = effectManager.getEffects().getRandomBonusOrMalus();
                if (Main.getInstance().isBroadcastEffects()) {
                    Bukkit.broadcastMessage(Main.PREFIX + "L'effet choisi est : " + effect);
                } else {
                    player.sendMessage(Main.PREFIX + "L'effet choisi est : " + effect);
                }

                break;
        }
        return true;
    }

    @Override
    protected String generateDescription(Player player) {
        return DESCRIPTION;
    }
}
