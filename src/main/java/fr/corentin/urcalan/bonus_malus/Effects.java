package fr.corentin.urcalan.bonus_malus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Abstract class {@link Effects}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class Effects {
    private final List<String> bonus;
    private final List<String> malus;
    private final List<String> both;

    public Effects(List<String> bonus, List<String> malus) {
        this.bonus = bonus;
        this.malus = malus;
        this.both = new ArrayList<>();
        this.both.addAll(this.bonus);
        this.both.addAll(this.malus);
    }

    public void clear(){
        this.bonus.clear();
        this.malus.clear();
        this.both.clear();
    }

    public String getRandomBonus() {
        if (this.bonus.isEmpty()) {
            throw new IllegalArgumentException("Error! The list of bonus is empty.");
        }
        return this.bonus.get(new Random().nextInt(this.bonus.size()));
    }

    public String getRandomMalus() {
        if (this.malus.isEmpty()) {
            throw new IllegalArgumentException("Error! The list of malus is empty.");
        }
        return this.malus.get(new Random().nextInt(this.malus.size()));
    }

    public String getRandomBonusOrMalus() {
        if (this.both.isEmpty()) {
            throw new IllegalArgumentException("Error! The list of bonus and malus is empty.");
        }
        return this.both.get(new Random().nextInt(this.both.size()));
    }

    public List<String> getBonus() {
        return bonus;
    }

    public List<String> getMalus() {
        return malus;
    }

    public List<String> getBoth() {
        return both;
    }
}