package fr.corentin.urcalan.utils;

import fr.corentin.urcalan.Main;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class {@link CustomConfig}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class CustomConfig {
    private String filename;
    private File file;
    private YamlConfiguration config;
    private String[] data;

    /**
     * Constructeur sans data d'une config
     *
     * @param filename Nom du fichier de config
     */
    public CustomConfig(String filename) {
        this.filename = filename;
        setup();
    }

    /**
     * Constructeur avec data d'une config
     *
     * @param filename Nom du fichier de config
     * @param data     Contenu du fichier de config
     */
    public CustomConfig(String filename, String... data) {
        this.filename = filename;
        this.data = data.clone();
        setup();
    }

    /**
     * Créer le fichier de config avec ou sans data
     */
    private void setup() {
        if (!Main.getInstance().getDataFolder().exists()) {
            Main.getInstance().getDataFolder().mkdir();
        }
        File file = new File(Main.getInstance().getDataFolder(), filename);

        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.file = file;
            config = YamlConfiguration.loadConfiguration(this.file);
            if (data != null) {
                for (String str : data) {
                    String[] splitted = str.split(": ");
                    String section = "";
                    String set = splitted[0];
                    if (set.contains("\\.")) {
                        String[] setAndSection = set.split("\\.");
                        section = setAndSection[1];
                        if (!config.isConfigurationSection(section)) config.createSection(section);
                        section = ".";
                    }
                    try {
                        config.set(section + set, Double.parseDouble(splitted[1]));
                    } catch (NumberFormatException e) {
                        config.set(section + set, splitted[1]);
                    }
                }
            }
        } else {
            this.file = file;
            config = YamlConfiguration.loadConfiguration(file);
        }
        save();
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getPaths() {
        Iterator<String> ite = this.getConfig().getKeys(false).iterator();
        ArrayList<String> paths = new ArrayList<>();
        while (ite.hasNext()) {
            String currentValue = ite.next();
            paths.add(currentValue);
        }
        return paths;
    }

    public YamlConfiguration getConfig() {
        return config;
    }

}
