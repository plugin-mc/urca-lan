package fr.corentin.urcalan.utils;

import fr.corentin.urcalan.Main;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Class {@link FileUtils}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class FileUtils {

    public static void createFiles(String... files) {
        for (String s : files) {
            String filePath = Main.getInstance().getDataFolder().getAbsolutePath() + s;
            File file = new File(filePath);
            if (!file.exists()) {
                try {
                    if(file.createNewFile()){
                        Bukkit.getLogger().log(Level.INFO, "The file " + file + " has been created.");
                    }
                } catch (IOException e) {
                    throw new IllegalStateException("Couldn't create file", e);
                }
            }
        }
    }
}
