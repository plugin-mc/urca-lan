package fr.corentin.urcalan;

import fr.corentin.urcalan.bonus_malus.Effects;
import fr.corentin.urcalan.bonus_malus.EffectsManager;
import fr.corentin.urcalan.chest.ChestManager;
import fr.corentin.urcalan.chest.LootableItemStack;
import fr.corentin.urcalan.json.EffectsSerializer;
import fr.corentin.urcalan.json.LootableItemStackSerializer;
import fr.corentin.urcalan.parents.IModule;
import fr.skytale.commandlib.Commands;
import fr.skytale.jsonlib.JsonManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class {@link Main}
 *
 * @author Corentin
 * @version URCA's lan
 */
public final class Main extends JavaPlugin {
    public static final String PREFIX = ChatColor.GOLD + "[Lan]" + ChatColor.GRAY + " >> " + ChatColor.WHITE;
    private static Main instance;

    private final List<IModule> modules = new ArrayList<>();

    private Commands commandsManager;
    private JsonManager jsonManager;

    private EffectsManager effectManager;
    private ChestManager chestManager;


    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        //JSON Manager
        this.jsonManager = new JsonManager();
        this.jsonManager.addAdapter(Effects.class, new EffectsSerializer());
        this.jsonManager.addAdapter(LootableItemStack.class, new LootableItemStackSerializer());

        //Command manager
        this.commandsManager = Commands.setup(Objects.requireNonNull(this.getCommand("urcalan")), PREFIX);

        this.effectManager = new EffectsManager();
        this.chestManager = new ChestManager();

        registerModules();
        enableModules();
    }

    @Override
    public void onDisable() {
        disableModules();
        modules.clear();
    }

    private void registerModules() {

        modules.add(this.effectManager);
        modules.add(this.chestManager);
    }

    public void enableModules() {
        this.jsonManager.enable();
        modules.forEach(IModule::enable);
    }

    public void disableModules() {
        modules.forEach(IModule::disable);
    }

    public static Main getInstance() {
        return instance;
    }

    public JsonManager getJsonManager() {
        return this.jsonManager;
    }

    public Commands getCommandsManager() {
        return commandsManager;
    }

    public EffectsManager getEffectManager() {
        return effectManager;
    }

    public ChestManager getChestManager() {
        return chestManager;
    }

    public World getWorld() {
        return Bukkit.getWorld(Objects.requireNonNull(this.getConfig().getString("world")));
    }

    public World getEndWorld() {
        return Bukkit.getWorld(Objects.requireNonNull(this.getConfig().getString("end_world")));
    }

    public World getNetherWorld() {
        return Bukkit.getWorld(Objects.requireNonNull(this.getConfig().getString("nether_world")));
    }

    public boolean isBroadcastEffects(){
        return this.getConfig().getBoolean("broadcast_message_effects");
    }

    public boolean isBroadcastChests(){
        return this.getConfig().getBoolean("broadcast_message_chests");
    }
}
