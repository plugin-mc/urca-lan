package fr.corentin.urcalan.json;

import com.google.gson.*;
import fr.corentin.urcalan.chest.LootableItemStack;
import fr.skytale.jsonlib.ISerializer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Class {@link LootableItemStackSerializer}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class LootableItemStackSerializer implements ISerializer<LootableItemStack> {
    @Override
    public LootableItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject obj = jsonElement.getAsJsonObject();

        Material material = Material.valueOf(obj.get("material").getAsString());
        int cumulativeProbability = obj.get("cumulative_probability").getAsInt();
        int amount = obj.get("amount").getAsInt();
        int durability = obj.get("durability").getAsInt();


        if (obj.has("enchants") && !obj.get("enchants").getAsString().equals("") &&
                obj.has("enchant_level") && !obj.get("enchant_level").getAsString().equals("")) {
            String enchantsStr = obj.get("enchants").getAsString();
            ArrayList<String> enchantsListStr = new ArrayList<>(Arrays.asList(enchantsStr.split(",")));
            ArrayList<Enchantment> enchants = new ArrayList<>();

            enchantsListStr.forEach(enchantStr -> {
                enchants.add(EnchantmentWrapper.getByName(enchantStr));
            });

            String enchantsLevelStr = obj.get("enchant_level").getAsString();
            ArrayList<Integer> enchantLevels = new ArrayList<>();
            if (isStringInt(enchantsLevelStr)) {
                enchantLevels.add(Integer.parseInt(enchantsLevelStr));
            } else {
                ArrayList<String> enchantsLevelListStr = new ArrayList<>(Arrays.asList(enchantsLevelStr.split(",")));
                enchantsLevelListStr.forEach(enchantLevel -> {
                    enchantLevels.add(Integer.valueOf(enchantLevel));
                });
            }


            return new LootableItemStack(material, durability, amount, cumulativeProbability, enchants, enchantLevels);

        }
        return new LootableItemStack(material, durability, amount, cumulativeProbability);
    }


    @Override
    public JsonElement serialize(LootableItemStack lootableItemStack, Type type, JsonSerializationContext jsonSerializationContext) {
        return null;
    }

    private boolean isStringInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }


}
