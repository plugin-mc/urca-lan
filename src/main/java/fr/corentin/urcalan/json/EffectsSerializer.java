package fr.corentin.urcalan.json;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import fr.corentin.urcalan.bonus_malus.Effects;
import fr.skytale.jsonlib.ISerializer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Class {@link EffectsSerializer}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class EffectsSerializer implements ISerializer<Effects> {
    @Override
    public Effects deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Type listType = new TypeToken<List<String>>() {}.getType();
        List<String> effects_bonus = new Gson().fromJson(json.getAsJsonObject().get("bonus").getAsString(), listType);
        List<String> effects_malus = new Gson().fromJson(json.getAsJsonObject().get("malus").getAsString(), listType);
        return new Effects(effects_bonus, effects_malus);
    }

    @Override
    public JsonElement serialize(Effects src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("bonus", new Gson().toJson(src.getBonus()));
        jsonObject.addProperty("malus", new Gson().toJson(src.getMalus()));
        return jsonObject;
    }
}
