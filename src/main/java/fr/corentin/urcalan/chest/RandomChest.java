package fr.corentin.urcalan.chest;

import fr.corentin.urcalan.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Class {@link RandomChest}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class RandomChest {

    private final int number;
    private final Inventory inventory;
    private final Location location;
    private static final int MIN_ITEMS_NB = 6;
    private static final int MAX_ITEMS_NB = 11;

    RandomChest(int number, Location location) {
        this.number = number;
        inventory = Bukkit.createInventory(null, InventoryType.CHEST);
        int nbItems = (int) Math.floor(Math.random() * (MAX_ITEMS_NB - MIN_ITEMS_NB + 1) + MIN_ITEMS_NB);
        int i = 0;
        while (i <= nbItems) {
            addItemRandomly();
            i++;
        }
        this.location = location;
    }


    private ItemStack getNewItem() {
        int proba = (int) (Math.random() * 10000);
        ArrayList<LootableItemStack> itemStacks = Main.getInstance().getChestManager().getItemStackProbabilities();
        int previousProba = 0;
        for (LootableItemStack itemStack : itemStacks) {
            if (proba >= previousProba && proba <= itemStack.getCumulativeProbability()) {
                return itemStack.getItemStack();
            }
        }
        return new ItemStack(Material.AIR);
    }

    private void addItemRandomly() {
        boolean placed = false;
        while (!placed) {
            int slot = (int) (Math.random() * 26);
            if (inventory.getItem(slot) == null) {
                inventory.setItem(slot, getNewItem());
                placed = true;
            }
        }

    }

    public int getNumber() {
        return number;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Location getLocation() {
        return location;
    }

}
