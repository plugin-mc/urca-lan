package fr.corentin.urcalan.chest;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.corentin.urcalan.Main;
import fr.corentin.urcalan.parents.IModule;
import fr.corentin.urcalan.utils.CustomConfig;
import fr.corentin.urcalan.utils.FileUtils;
import fr.skytale.jsonlib.JsonManager;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Class {@link ChestManager}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class ChestManager implements IModule {

    private CustomConfig config;
    private ArrayList<Location> chestsLocations;
    private ArrayList<LootableItemStack> itemStackProbabilities;


    @Override
    public void enable() {
        JsonManager jsonManager = Main.getInstance().getJsonManager();
        FileUtils.createFiles("/chests_probabilities.json");
        String filePath = Main.getInstance().getDataFolder().getAbsolutePath() + "/chests_probabilities.json";
        File file = new File(filePath);

        itemStackProbabilities = new ArrayList<>();

        config = new CustomConfig("chest_cfg.yml");
        chestsLocations = new ArrayList<>();
        chestsLocations = loadChestsLocationsFromConfig();
        itemStackProbabilities = new ArrayList<>();

        try {
            String jsonString = Files.readString(Paths.get(file.getAbsolutePath()));
            JsonParser parser = new JsonParser();
            JsonArray array = parser.parse(jsonString).getAsJsonArray();
            for (JsonElement element : array) {
                JsonObject jObj = element.getAsJsonObject();
                LootableItemStack lootableItemStack = jsonManager.deserialize(jObj.toString(), LootableItemStack.class);
                itemStackProbabilities.add(lootableItemStack);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disable() {
        chestsLocations.clear();
        itemStackProbabilities.clear();
    }

    @Override
    public void registerCommand() {

    }

    @Override
    public void registerListener() {

    }

    public ArrayList<Location> loadChestsLocationsFromConfig() {

        ArrayList<Location> chestsLocations = new ArrayList<>();
        if (config.getConfig().get("chests") == null) {
            return chestsLocations;
        }
        for (String key : Objects.requireNonNull(config.getConfig().getConfigurationSection("chests")).getKeys(false)) {
            String section = "chests." + key + ".";
            double x = config.getConfig().getDouble(section + "x");
            double y = config.getConfig().getDouble(section + "y");
            double z = config.getConfig().getDouble(section + "z");
            chestsLocations.add(new Location(Main.getInstance().getWorld(), x, y, z));
        }
        return chestsLocations;
    }

    public void saveChestsLocationToConfig(ArrayList<Location> locations) {
        this.chestsLocations.clear();
        config.getConfig().set("chests", null);
        for (int i = 0; i < locations.size(); i++) {
            int x = locations.get(i).getBlockX();
            int y = locations.get(i).getBlockY();
            int z = locations.get(i).getBlockZ();
            String configSection = "chests." + (i + 1);
            config.getConfig().set(configSection + ".x", x);
            config.getConfig().set(configSection + ".y", y);
            config.getConfig().set(configSection + ".z", z);
            this.chestsLocations.add(new Location(Main.getInstance().getWorld(), x, y, z));
        }
        config.save();
    }

    public ArrayList<Location> getChestsLocations() {
        return chestsLocations;
    }

    public ArrayList<LootableItemStack> getItemStackProbabilities() {
        return itemStackProbabilities;
    }
}
