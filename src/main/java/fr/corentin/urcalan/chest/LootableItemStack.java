package fr.corentin.urcalan.chest;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Class {@link LootableItemStack}
 *
 * @author Corentin
 * @version URCA's lan
 */
public class LootableItemStack {
    private Material material;
    private int durabilty;
    private int amount;
    private ArrayList<Enchantment> enchantments;
    private ArrayList<Integer> enchantmentsLevels;
    private int cumulativeProbability;

    private ItemStack itemStack;

    public LootableItemStack(Material material, int durabilty, int amount, int cumulativeProbability, ArrayList<Enchantment> enchantments,
                             ArrayList<Integer> enchantmentsLevels) {
        this.material = material;
        this.durabilty = durabilty;
        this.enchantmentsLevels = enchantmentsLevels;
        this.amount = amount;
        this.enchantments = enchantments;
        this.cumulativeProbability = cumulativeProbability;

        itemStack = new ItemStack(material, amount, (short) durabilty);
        if (!enchantments.isEmpty()) {
            if (material == Material.ENCHANTED_BOOK) {
                EnchantmentStorageMeta itemStackM = (EnchantmentStorageMeta) itemStack.getItemMeta();
                for (int i = 0; i < enchantments.size(); i++) {
                    assert itemStackM != null;
                    itemStackM.addStoredEnchant(enchantments.get(i), enchantmentsLevels.get(i), false);
                }
                itemStack.setItemMeta(itemStackM);
            } else {
                ItemMeta itemStackM = itemStack.getItemMeta();
                for (int i = 0; i < enchantments.size(); i++) {
                    assert itemStackM != null;
                    itemStackM.addEnchant(enchantments.get(i), enchantmentsLevels.get(i), false);
                }
                itemStack.setItemMeta(itemStackM);
            }
        }
    }

    public LootableItemStack(Material material, int durabilty, int amount, int cumulativeProbability) {
        this.material = material;
        this.durabilty = durabilty;
        this.amount = amount;
        this.cumulativeProbability = cumulativeProbability;

        itemStack = new ItemStack(material, amount);

    }


    public ItemStack getItemStack() {
        return itemStack;
    }


    @Override
    public String toString() {
        return "LootableItemStack{" +
                "material=" + material +
                ", durabilty=" + durabilty +
                ", amount=" + amount +
                ", enchantments=" + enchantments +
                ", enchantmentsLevels=" + enchantmentsLevels +
                ", cumulativeProbability=" + cumulativeProbability +
                '}';
    }

    public int getCumulativeProbability() {
        return cumulativeProbability;
    }

}
