package fr.corentin.urcalan.parents;

/**
 * Interface {@link IModule}
 *
 * @author Corentin
 * @version URCA's lan
 */
public interface IModule {
    void enable();

    void disable();

    void registerCommand();

    void registerListener();
}
