# URCA-Lan

- **Plugin pour la LAN 2022 de L'URCA.** *(Je tiens à m'excuser, le plugin n'est pas complet mais étant malade et avec la reprise des cours je n'ai pas eu trop le temps. Mais j'en ai fait un maximum 😉 )*

## Auteur

- [D. Corentin](https://twitter.com/Eowalim) - Étudiant de troisième année

## Version Minecraft

- Version : **1.18.X**

## Pré-requis

- **Java 17** sur la machine ou le serveur MC est présent *(obligatoire pour la 1.18.X)*

## Configuration

1) Mettre le plugin dans le dossier ``/plugins``.
2) Lancer le serveur une première fois.
3) Éteindre le serveur une fois qu'il a fini de se load càd quand c'est écrit dans la console : ``Done (x.xxxs)! For help, type "help"``
4) Éditer le fichier ``config.yml`` présent dans ``/plugins/URCALan/`` :

| Champs | Description |
| :---: | :---: |
| ``broadcast_message_effets`` | Active ou non le fait d'envoyer un message à tous les joueurs pour annoncer **le nom du bonus ou malus tié au sort**. (**true** -> activé / **false** -> désactivé) |
| ``broadcast_message_chests`` |  Active ou non le fait d'envoyer un message à tous les joueurs pour annoncer **l'emplacement du coffre spawné**. (**true** -> activé / **false** -> désactivé)  |
| ``world`` | Nom du dossier qui correspond au monde de **l'overworld** dans le dossier du serveur. |
| ``nether_world`` | Nom du dossier qui correspond au monde du **nether** dans le dossier du serveur.  | 
| ``end_world`` | Nom du dossier qui correspond au monde de **l'end** dans le dossier du serveur.| 


### Bonus - Malus

- Aller dans le dossier ``/plugins/URCALan/``.
- Éditer le fichier JSON ``effects.json``

**Si le fichier contient déjà des valeurs, vous pouvez repartir de 0 avec les exemples ci-dessous :**

**effects.json :**
```json
{
  "bonus": [
    "Bonus 1",
    "Bonus 2",
    "Bonus 3"
  ],
  "malus": [
    "Malus 1",
    "Malus 2",
    "Malus 3"
  ]
}
```

**N'oubliez pas d'enregistrer et de relancer le serveur après modification !**

#### Commandes

- Les **commandes** sont **utilisables** par les **joueurs opérateurs**.

| Commande | Description | Permission |
| :---: | :---: | :---: | 
| ``/urcalan get malus`` | Renvois au joueur qui a exécuté la commande le nom d'un **malus** | OP |
| ``/urcalan get bonus`` | Renvois au joueur qui a exécuté la commande le nom d'un **bonus**  | OP |
| ``/urcalan get random`` | Renvois au joueur qui a exécuté la commande le nom d'un **bonus** ou d'un **malus**| OP |


### Coffres


## Crédits

> Étant membre de l'association [Skytale](https://skytale.fr/), j'ai eu l'autorisation d'utiliser certaines de leurs librairies.



